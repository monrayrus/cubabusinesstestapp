package com.company.cubabusinesstestapp.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Table(name = "CUBABUSINESSTESTAPP_UNIT")
@Entity(name = "cubabusinesstestapp_Unit")
@NamePattern("%s|title")
public class Unit extends StandardEntity {
    private static final long serialVersionUID = 4994626314535607927L;

    @NotNull
    @Column(name = "UNIT_ID", nullable = false, unique = true)
    private String unitId;

    @NotNull
    @Column(name = "TITLE", nullable = false)
    private String title;


    //todo реализовать проверку(валидацию?) закольцованности
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PARENT_UNIT_ID")
    private Unit parentUnit;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "MANAGER_ID")
    private Employee manager;

    public Employee getManager() {
        return manager;
    }

    public void setManager(Employee manager) {
        this.manager = manager;
    }

    public Unit getParentUnit() {
        return parentUnit;
    }

    public void setParentUnit(Unit parentUnit) {
        this.parentUnit = parentUnit;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUnitId() {
        return unitId;
    }

    public void setUnitId(String unitId) {
        this.unitId = unitId;
    }
}