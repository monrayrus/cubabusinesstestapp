package com.company.cubabusinesstestapp.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Table(name = "CUBABUSINESSTESTAPP_DOCUMENT_TYPE")
@Entity(name = "cubabusinesstestapp_DocumentType")
@NamePattern("%s|title")
public class DocumentType extends StandardEntity {
    private static final long serialVersionUID = 2270337369016390384L;

    @NotNull
    @Column(name = "DOCUMENT_ID", nullable = false, unique = true)
    private String documentId;

    @NotNull
    @Column(name = "TITLE", nullable = false)
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }
}