package com.company.cubabusinesstestapp.entity;

import com.haulmont.chile.core.annotations.NamePattern;
import com.haulmont.cuba.core.entity.StandardEntity;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;

@Table(name = "CUBABUSINESSTESTAPP_DOCUMENT")
@Entity(name = "cubabusinesstestapp_Document")
@NamePattern("%s|title")
public class Document extends StandardEntity {
    private static final long serialVersionUID = 6180807168184614267L;

    @NotNull
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "DOCUMENT_TYPE_ID")
    private DocumentType documentType;

    @NotNull
    @Column(name = "REGISTRY_NUMBER", nullable = false, unique = true)
    private String registryNumber;

    @Temporal(TemporalType.DATE)
    @Column(name = "REGISTRY_DATE")
    private Date registryDate;

    @Column(name = "TO_WHOM")
    private String toWhom;

    @NotNull
    @Column(name = "TOPIC", nullable = false)
    private String topic;

    @Column(name = "ADDRESSEE")
    private String addressee;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "PERFORMER_ID")
    private Employee performer;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "SIGNER_ID")
    private Employee signer;

    @Column(name = "NOTE")
    private String note;

    @NotNull
    @Column(name = "TITLE", nullable = false)
    private String title;

    @Column(name = "STATE")
    private String state;

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public Employee getSigner() {
        return signer;
    }

    public void setSigner(Employee signer) {
        this.signer = signer;
    }

    public Employee getPerformer() {
        return performer;
    }

    public void setPerformer(Employee performer) {
        this.performer = performer;
    }

    public String getAddressee() {
        return addressee;
    }

    public void setAddressee(String addressee) {
        this.addressee = addressee;
    }

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public String getToWhom() {
        return toWhom;
    }

    public void setToWhom(String toWhom) {
        this.toWhom = toWhom;
    }

    public Date getRegistryDate() {
        return registryDate;
    }

    public void setRegistryDate(Date registryDate) {
        this.registryDate = registryDate;
    }

    public String getRegistryNumber() {
        return registryNumber;
    }

    public void setRegistryNumber(String registryNumber) {
        this.registryNumber = registryNumber;
    }

    public DocumentType getDocumentType() {
        return documentType;
    }

    public void setDocumentType(DocumentType documentType) {
        this.documentType = documentType;
    }
}