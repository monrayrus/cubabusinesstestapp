package com.company.cubabusinesstestapp.service;

public interface TableIdGeneratorService {
    String NAME = "cubabusinesstestapp_TableIdGeneratorService";

    public String generateId(String prefix, String modelType);
}