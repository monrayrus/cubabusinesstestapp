package com.company.cubabusinesstestapp.service;

import com.company.cubabusinesstestapp.entity.Employee;
import com.haulmont.cuba.security.entity.User;

public interface UserCheckerService {
    String NAME = "cubabusinesstestapp_UserCheckerService";

    Employee getEmployeeByUser(User user);
}