package com.company.cubabusinesstestapp.service;

import com.company.cubabusinesstestapp.entity.Employee;
import com.haulmont.cuba.security.entity.User;

import java.util.Optional;

public interface EmployeeFromUserService {
    String NAME = "cubabusinesstestapp_EmployeeFromUserService";

    Employee getEmployeeFromUser(User user);
}