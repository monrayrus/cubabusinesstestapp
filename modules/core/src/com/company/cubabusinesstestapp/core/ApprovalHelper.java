package com.company.cubabusinesstestapp.core;

import com.company.cubabusinesstestapp.entity.Document;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Transaction;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.UUID;

@Component(ApprovalHelper.NAME)
public class ApprovalHelper {
    public static final String NAME = "cubabusinesstestapp_ApprovalHelper";

    @Inject
    private Persistence persistence;

    public void updateState(UUID entityId, String state) {
        try (Transaction tx = persistence.getTransaction()) {
            Document document = persistence.getEntityManager().find(Document.class, entityId);
            if (document != null) {
                document.setState(state);
            }
            tx.commit();
        }
    }
}