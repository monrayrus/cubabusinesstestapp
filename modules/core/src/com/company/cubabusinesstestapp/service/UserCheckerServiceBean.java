package com.company.cubabusinesstestapp.service;

import com.company.cubabusinesstestapp.entity.Employee;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.security.entity.User;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service(UserCheckerService.NAME)
public class UserCheckerServiceBean implements UserCheckerService {
    @Inject
    private DataManager dataManager;

    public Employee getEmployeeByUser(User user) {
        return dataManager.load(Employee.class)
                .query("select e from cubabusinesstestapp_Employee e where e.user_id = :uuid")
                .parameter("uuid", user.getUuid())
                .one();
    }
}