package com.company.cubabusinesstestapp.service;

import com.haulmont.cuba.core.app.UniqueNumbersService;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service(TableIdGeneratorService.NAME)
public class TableIdGeneratorServiceBean implements TableIdGeneratorService {

    @Inject
    private UniqueNumbersService service;

    @Override
    public String generateId(String prefix, String modelType) {
        long id = service.getNextNumber(modelType);
        String result = prefix + String.format("%06d %n", id);
        return result;
    }
}