package com.company.cubabusinesstestapp.service;

import com.company.cubabusinesstestapp.entity.Employee;
import com.haulmont.cuba.core.global.DataManager;
import com.haulmont.cuba.security.entity.User;
import org.springframework.stereotype.Service;

import javax.inject.Inject;

@Service(EmployeeFromUserService.NAME)
public class EmployeeFromUserServiceBean implements EmployeeFromUserService {

    @Inject
    private DataManager dataManager;

    @Override
    public Employee getEmployeeFromUser(User user) {
        return dataManager.load(Employee.class)
                .query("select e from cubabusinesstestapp_Employee e where e.user.id = :userid")
                .parameter("userid", user.getUuid())
                .one();
    }
}