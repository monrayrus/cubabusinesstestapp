-- begin CUBABUSINESSTESTAPP_DOCUMENT
create table CUBABUSINESSTESTAPP_DOCUMENT (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    DOCUMENT_TYPE_ID uuid not null,
    REGISTRY_NUMBER varchar(255) not null,
    REGISTRY_DATE date,
    TO_WHOM varchar(255),
    TOPIC varchar(255) not null,
    ADDRESSEE varchar(255),
    PERFORMER_ID uuid,
    SIGNER_ID uuid,
    NOTE varchar(255),
    TITLE varchar(255) not null,
    STATE varchar(255),
    --
    primary key (ID)
)^
-- end CUBABUSINESSTESTAPP_DOCUMENT
-- begin CUBABUSINESSTESTAPP_UNIT
create table CUBABUSINESSTESTAPP_UNIT (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    UNIT_ID varchar(255) not null,
    TITLE varchar(255) not null,
    PARENT_UNIT_ID uuid,
    MANAGER_ID uuid,
    --
    primary key (ID)
)^
-- end CUBABUSINESSTESTAPP_UNIT
-- begin CUBABUSINESSTESTAPP_EMPLOYEE
create table CUBABUSINESSTESTAPP_EMPLOYEE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    TABLE_NUMBER varchar(255) not null,
    USER_ID uuid not null,
    LAST_NAME varchar(255) not null,
    FIRST_NAME varchar(255) not null,
    MIDDLE_NAME varchar(255),
    UNIT_ID uuid not null,
    EMAIL varchar(255),
    PHONE_NUMBER varchar(255),
    IMAGE_ID uuid,
    --
    primary key (ID)
)^
-- end CUBABUSINESSTESTAPP_EMPLOYEE
-- begin CUBABUSINESSTESTAPP_DOCUMENT_TYPE
create table CUBABUSINESSTESTAPP_DOCUMENT_TYPE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    DOCUMENT_ID varchar(255) not null,
    TITLE varchar(255) not null,
    --
    primary key (ID)
)^
-- end CUBABUSINESSTESTAPP_DOCUMENT_TYPE
