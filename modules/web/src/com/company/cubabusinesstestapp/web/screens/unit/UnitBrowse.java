package com.company.cubabusinesstestapp.web.screens.unit;

import com.haulmont.cuba.gui.screen.*;
import com.company.cubabusinesstestapp.entity.Unit;

@UiController("cubabusinesstestapp_Unit.browse")
@UiDescriptor("unit-browse.xml")
@LookupComponent("unitsTable")
@LoadDataBeforeShow
public class UnitBrowse extends StandardLookup<Unit> {
}