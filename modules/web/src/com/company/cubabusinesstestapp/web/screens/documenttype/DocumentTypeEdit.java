package com.company.cubabusinesstestapp.web.screens.documenttype;

import com.company.cubabusinesstestapp.service.TableIdGeneratorService;
import com.haulmont.cuba.gui.screen.*;
import com.company.cubabusinesstestapp.entity.DocumentType;

import javax.inject.Inject;

@UiController("cubabusinesstestapp_DocumentType.edit")
@UiDescriptor("document-type-edit.xml")
@EditedEntityContainer("documentTypeDc")
@LoadDataBeforeShow
public class DocumentTypeEdit extends StandardEditor<DocumentType> {
    private static final String MODEL_PREFIX = "ВД";
    private static final String MODEL_TYPE = "DocumentType";

    @Inject
    private TableIdGeneratorService tableIdGeneratorService;

    @Subscribe
    public void onInitEntity(InitEntityEvent<DocumentType> event) {
        String generatedId = tableIdGeneratorService.generateId(MODEL_PREFIX, MODEL_TYPE);
        event.getEntity().setDocumentId(generatedId);
    }

}