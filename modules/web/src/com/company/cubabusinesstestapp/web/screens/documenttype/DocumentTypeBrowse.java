package com.company.cubabusinesstestapp.web.screens.documenttype;

import com.haulmont.cuba.gui.screen.*;
import com.company.cubabusinesstestapp.entity.DocumentType;

@UiController("cubabusinesstestapp_DocumentType.browse")
@UiDescriptor("document-type-browse.xml")
@LookupComponent("documentTypesTable")
@LoadDataBeforeShow
public class DocumentTypeBrowse extends StandardLookup<DocumentType> {
}