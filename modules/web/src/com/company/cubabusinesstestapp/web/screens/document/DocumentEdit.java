package com.company.cubabusinesstestapp.web.screens.document;

import com.company.cubabusinesstestapp.service.EmployeeFromUserService;
import com.company.cubabusinesstestapp.service.TableIdGeneratorService;
import com.haulmont.bpm.entity.ProcAttachment;
import com.haulmont.bpm.gui.procactionsfragment.ProcActionsFragment;
import com.haulmont.cuba.gui.app.core.file.FileDownloadHelper;
import com.haulmont.cuba.gui.components.Button;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.components.TextInputField;
import com.haulmont.cuba.gui.model.CollectionLoader;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.model.InstanceLoader;
import com.haulmont.cuba.gui.screen.*;
import com.company.cubabusinesstestapp.entity.Document;
import com.haulmont.cuba.security.global.UserSession;
import org.slf4j.Logger;

import javax.inject.Inject;
import java.util.Date;

@UiController("cubabusinesstestapp_Document.edit")
@UiDescriptor("document-edit.xml")
@EditedEntityContainer("documentDc")
@LoadDataBeforeShow
public class DocumentEdit extends StandardEditor<Document> {


    @Inject
    private Logger log;

    private static final String MODEL_PREFIX = "Д";

    private static final String MODEL_TYPE = "Document";

    //Process definition code BPM
    private static final String PROCESS_CODE = "businessLogic";

    @Inject
    private TableIdGeneratorService service;

    @Inject
    private UserSession userSession;

    @Inject
    private CollectionLoader<ProcAttachment> procAttachmentsDl;

    @Inject
    private InstanceContainer<Document> documentDc;

    @Inject
    protected ProcActionsFragment procActionsFragment;

    @Inject
    private Table<ProcAttachment> attachmentsTable;

    @Inject
    private InstanceLoader<Document> documentDl;

    @Inject
    private EmployeeFromUserService efuService;

    @Subscribe
    private void onBeforeShow(BeforeShowEvent event) {
        documentDl.load();
        procAttachmentsDl.setParameter("entityId",documentDc.getItem().getId());
        procAttachmentsDl.load();
        procActionsFragment.initializer()
                .standard()
                .init(PROCESS_CODE, getEditedEntity());

        FileDownloadHelper.initGeneratedColumn(attachmentsTable, "file");
    }

    @Subscribe
    public void onInitEntity(InitEntityEvent<Document> event) {
        event.getEntity().setRegistryDate(new Date());
        event.getEntity().setRegistryNumber(service.generateId(MODEL_PREFIX, MODEL_TYPE));
        event.getEntity().setState("NEW");
        event.getEntity().setPerformer(efuService.getEmployeeFromUser(userSession.getUser()));
    }

    @Subscribe("titleField")
    public void onTitleFieldEnterPress(TextInputField.EnterPressEvent event) {
        event.getSource().setValue(documentDc.getItem().getDocumentType().getTitle() + ", №"
                + documentDc.getItem().getRegistryNumber() + " от "
                + documentDc.getItem().getRegistryDate() + " в "
                + documentDc.getItem().getAddressee() + ", "
                + documentDc.getItem().getTopic());
    }



}