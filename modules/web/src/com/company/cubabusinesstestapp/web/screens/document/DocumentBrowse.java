package com.company.cubabusinesstestapp.web.screens.document;

import com.company.cubabusinesstestapp.entity.Document;
import com.haulmont.bpm.entity.ProcAttachment;
import com.haulmont.cuba.gui.components.Table;
import com.haulmont.cuba.gui.export.ExportDisplay;
import com.haulmont.cuba.gui.screen.*;

import javax.inject.Inject;



@UiController("cubabusinesstestapp_Document.browse")
@UiDescriptor("document-browse.xml")
@LookupComponent("documentsTable")
@LoadDataBeforeShow
public class DocumentBrowse extends StandardLookup<Document> {

    @Inject
    private ExportDisplay exportDisplay;

    @Subscribe("procAttachmentsTable.file")
    public void onProcAttachmentsTableFileClick(Table.Column.ClickEvent<ProcAttachment> event) {
        exportDisplay.show(event.getItem().getFile());
    }
}
