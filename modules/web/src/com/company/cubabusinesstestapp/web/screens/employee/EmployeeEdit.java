package com.company.cubabusinesstestapp.web.screens.employee;

import com.company.cubabusinesstestapp.service.TableIdGeneratorService;
import com.company.cubabusinesstestapp.service.UserCheckerService;
import com.haulmont.cuba.gui.components.HasValue;
import com.haulmont.cuba.gui.components.PickerField;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.model.InstancePropertyContainer;
import com.haulmont.cuba.gui.screen.*;
import com.company.cubabusinesstestapp.entity.Employee;
import com.haulmont.cuba.security.entity.User;
import org.slf4j.Logger;

import javax.inject.Inject;


@UiController("cubabusinesstestapp_Employee.edit")
@UiDescriptor("employee-edit.xml")
@EditedEntityContainer("employeeDc")
@LoadDataBeforeShow
public class EmployeeEdit extends StandardEditor<Employee> {
    Logger log;
    private static final String MODEL_PREFIX = "Р";
    private static final String MODEL_TYPE = "Employee";
    @Inject
    private TableIdGeneratorService tableIdGeneratorService;

    @Inject
    private InstanceContainer<Employee> employeeDc;
    @Inject
    private InstancePropertyContainer<User> userDc;
    @Inject
    private UserCheckerService checkerService;

    @Subscribe
    public void onInitEntity(InitEntityEvent<Employee> event) {
        String generatedId = tableIdGeneratorService.generateId(MODEL_PREFIX, MODEL_TYPE);
        event.getEntity().setTableNumber(generatedId);
    }


    @Subscribe("userField")
    public void onUserFieldValueChange(HasValue.ValueChangeEvent<User> event) {
        employeeDc.getItem().setFirstName(userDc.getItem().getFirstName());
        employeeDc.getItem().setLastName(userDc.getItem().getLastName());
        employeeDc.getItem().setMiddleName(userDc.getItem().getMiddleName());
    }


}

