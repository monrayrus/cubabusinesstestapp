package com.company.cubabusinesstestapp.web.screens.unit;

import com.company.cubabusinesstestapp.service.TableIdGeneratorService;
import com.haulmont.cuba.gui.components.ValidationException;
import com.haulmont.cuba.gui.model.InstanceContainer;
import com.haulmont.cuba.gui.screen.*;
import com.company.cubabusinesstestapp.entity.Unit;

import javax.inject.Inject;

@UiController("cubabusinesstestapp_Unit.edit")
@UiDescriptor("unit-edit.xml")
@EditedEntityContainer("unitDc")
@LoadDataBeforeShow
public class UnitEdit extends StandardEditor<Unit> {
    private static final String MODEL_PREFIX = "П";
    private static final String MODEL_TYPE = "Unit";
    @Inject
    private InstanceContainer<Unit> unitDc;
    @Inject
    private TableIdGeneratorService tableIdGeneratorService;

    @Subscribe
    public void onInitEntity(InitEntityEvent<Unit> event) {
        String generatedId = tableIdGeneratorService.generateId(MODEL_PREFIX, MODEL_TYPE);
        event.getEntity().setUnitId(generatedId);
    }

    @Install(to = "parentUnitField", subject = "validator")
    private void parentUnitFieldValidator(Unit unit) {
        if (unit != null) {
            boolean result = isLooped(unitDc.getItem(), unit);
            if (result) throw new ValidationException("Unit structure is looped");
        }
    }

    private boolean isLooped(Unit currentUnit, Unit candidate) {
        if (currentUnit.equals(candidate)) return true;
        while(candidate != null) {
            if(currentUnit.equals(candidate)) {
                return true;
            }
            candidate = candidate.getParentUnit();
        }
        return false;
    }
}